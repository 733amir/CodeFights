# isBinSymmetry = lambda x: x > 1 and bin(x)[2:][::-1] in bin(x)
# isBinSymmetry = lambda x: 0 < (bin(x)[2:][::-1] in bin(x)) and x > 1
isBinSymmetry = lambda x: x > 1 and bin(x)[:1:-1] in bin(x)

print(isBinSymmetry(1)) # False
print(isBinSymmetry(10)) # False
print(isBinSymmetry(51)) # True
print(isBinSymmetry(189)) # True


