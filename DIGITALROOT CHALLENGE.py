# d = lambda a : 1 + (int(a) - 1) % 9
# digitalRoot = lambda A, B: sorted(d(i + d(A)) for i in range((d(B) - d(A)) % 9 + 1))
# digitalRoot = lambda A, B: range(d(A), d(B) + 1) or range(1, d(B) + 1) + range(d(A), 10)
# digitalRoot = lambda A, B: sorted(map(d,range(d(A), d(A) + d(d(B) - d(A) + 1))))

# def digitalRoot(A, B):
#     a = int(A) % 9 or 9
#     b = int(B) % 9 or 9 + 1
#     r = range
#     return r(a, b) or r(1, b) + r(a, 10)

digitalRoot = lambda A, B: sorted((i + int(A)) % 9 or 9 for i in range((int(B) - int(A)) % 9 + 1))

print(digitalRoot('1', '5'))   # [1, 2, 3, 4, 5]
print(digitalRoot('5', '9'))   # [5, 6, 7, 8, 9]
print(digitalRoot('12', '22')) # [3, 4]
print(digitalRoot('9', '12'))  # [1, 2, 3, 9]
print(digitalRoot('7', '7'))   # [7]
print(digitalRoot('8', '22'))